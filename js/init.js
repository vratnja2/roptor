/**
 * Initialize the Game and starts it.
 */
var game = new Game();
var director = new Director();

function init() {

    game.init();
}

game.ready = window.setInterval(function(){checkReadyState()}, 1000);