function Weapon() {

    this.powerUpImage = ImageRepository.powerUpBoltRed;
    this.fire = function () {};
}


function SingleBeam() {

    this.name = "Single Beam";
    this.fireRate = 20;
    this.projectileSpeed = 12;
    this.powerUpImage = ImageRepository.powerUpBoltRed;
    this.damage = 1;

    this.fire = function() {
        game.player.bulletPool.get(game.player.x + 45, game.player.y, 0, -this.projectileSpeed);
        ObjectPools.playerLaserAudio.get();
    };
}

function DualBeam() {

    this.name = "Dual Beam";
    this.fireRate = 12;
    this.projectileSpeed = 12;
    this.damage = 1;
    this.side = 0;
    this.powerUpImage = ImageRepository.powerUpBoltRed;

    this.fire = function() {
        if (this.side == 0) {
            this.side = 1;
            game.player.bulletPool.get(game.player.x + 40, game.player.y, 0, -this.projectileSpeed);
        } else {
            this.side = 0;
            game.player.bulletPool.get(game.player.x + 50, game.player.y, 0, -this.projectileSpeed);
        }
        ObjectPools.playerLaserAudio.get();
    };
}

function TwinBlade() {

    this.name = "Twin Blade";
    this.fireRate = 12;
    this.projectileSpeed = 12;
    this.damage = 1;
    this.powerUpImage = ImageRepository.powerUpBoltRed2;

    this.fire = function() {
        game.player.bulletPool.get(game.player.x + 38, game.player.y, 0, -this.projectileSpeed);
        game.player.bulletPool.get(game.player.x + 52, game.player.y, 0, -this.projectileSpeed);
        ObjectPools.playerLaserAudio.get();
    };
}

function Hellfire() {

    this.name = "Hellfire";
    this.fireRate = 15;
    this.projectileSpeed = 12;
    this.damage = 1;
    this.powerUpImage = ImageRepository.powerUpBoltRed3;

    this.fire = function() {
        game.player.bulletPool.get(game.player.x + 38, game.player.y, 0, -this.projectileSpeed);
        game.player.bulletPool.get(game.player.x + 52, game.player.y, 0, -this.projectileSpeed);
        game.player.bulletPool.get(game.player.x - 8, game.player.y + 15, -2, -this.projectileSpeed, -0.25);
        game.player.bulletPool.get(game.player.x + 98, game.player.y + 15, 2, -this.projectileSpeed, 0.25);
        ObjectPools.playerLaserAudio.get();
    };
}

SingleBeam.prototype = new Weapon();
DualBeam.prototype = new Weapon();
TwinBlade.prototype = new Weapon();
Hellfire.prototype = new Weapon();

var WeaponRepository = new function() {

    this.init = function() {
        this.inventory = [];
        this.inventory["Single Beam"] = new SingleBeam();
        this.inventory["Dual Beam"] = new DualBeam();
        this.inventory["Twin Blade"] = new TwinBlade();
        this.inventory["Hellfire"] = new Hellfire();
    }

    this.init();
}