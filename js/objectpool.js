/**
 * Custom Pool object. Holds Bullet objects to be managed to prevent
 * garbage collection.
 */
var ObjectPools = new function() {

    var objectPools = [];
    var audioPools = [];

    this.init = function() {
        this.initObjects();
        this.initAudio();
    }

    this.initObjects = function() {
        this.enemies = new ObjectPool(20);
        this.enemies.init("enemy");
        objectPools[0] = this.enemies;
        game.enemyPool = this.enemies;

        this.asteroids = new ObjectPool(20);
        this.asteroids.init("asteroid");
        objectPools[1] = this.asteroids;
        game.asteroidPool = this.asteroids;

        this.asteroidsGrey = new ObjectPool(30);
        this.asteroidsGrey.init("asteroidGrey");
        objectPools[9] = this.asteroidsGrey;
        game.asteroidGreyPool = this.asteroidsGrey;

        this.enemyLaser = new ObjectPool(20);
        this.enemyLaser.init("enemyLaser");
        objectPools[2] = this.enemyLaser;
        game.enemyLaserPool = this.enemyLaser;

        this.weaponCrate = new ObjectPool(5);
        this.weaponCrate.init("weaponCrate");
        objectPools[3] = this.weaponCrate;
        game.weaponCratePool = this.weaponCrate;

        this.powerCrate = new ObjectPool(5);
        this.powerCrate.init("powerCrate");
        objectPools[8] = this.powerCrate;
        game.powerCratePool = this.powerCrate;

        objectPools[4] = game.player.bulletPool;

        this.enemies2 = new ObjectPool(5);
        this.enemies2.init("enemyDestroyer");
        objectPools[5] = this.enemies2;
        game.enemyDestroyerPool = this.enemies2;

        this.enemies3 = new ObjectPool(5);
        this.enemies3.init("enemyInterceptor");
        objectPools[10] = this.enemies3;
        game.enemyInterceptorPool = this.enemies3;

        this.enemies4 = new ObjectPool(2);
        this.enemies4.init("boss");
        objectPools[11] = this.enemies4;
        game.enemyBossPool = this.enemies4;

        this.explosion = new ObjectPool(10);
        this.explosion.init("explosion");
        objectPools[6] = this.explosion;
        game.explosionPool = this.explosion;

        this.asteroidExplosion = new ObjectPool(10);
        this.asteroidExplosion.init("asteroidExplosion");
        objectPools[7] = this.asteroidExplosion;
        game.asteroidExplosionPool = this.asteroidExplosion;
    }

    this.initAudio = function() {

        this.playerLaserAudio = new AudioPool(10);
        this.playerLaserAudio.init("playerLaser");
        audioPools[0] = this.playerLaserAudio;

        this.enemyLaserAudio = new AudioPool(20);
        this.enemyLaserAudio.init("enemyLaser");
        audioPools[1] = this.enemyLaserAudio;

        this.explosionAudio = new AudioPool(10);
        this.explosionAudio.init("explosionAudio");
        audioPools[2] = this.explosionAudio;
    }

    this.animate = function() {
        for(var i = 0; i < objectPools.length; i++) {
            objectPools[i].animate();
        }
    }
}

function ObjectPool(maxSize) {

    var size = maxSize; // Max bullets allowed in the pool
    var pool = [];

    /*
     * Populates the pool array with Bullet objects
     */
    this.init = function(object) {
        if (object == "laser") {
            for (var i = 0; i < size; i++) {
                // Initalize the object
                var bullet = new Bullet("laser");
                bullet.init(0, 0, ImageRepository.laser1);
                pool[i] = bullet;
            }
        }
        else if (object == "enemy") {
            for (var i = 0; i < size; i++) {
                var enemy = new Frigate();
                enemy.init(0, 0, ImageRepository.enemyShip1);
                pool[i] = enemy;
            }
        }
        else if (object == "enemyDestroyer") {
            for (var i = 0; i < size; i++) {
                var enemy = new Destroyer();
                enemy.init(0, 0, ImageRepository.enemyShip2);
                pool[i] = enemy;
            }
        }
        else if (object == "enemyInterceptor") {
            for (var i = 0; i < size; i++) {
                var enemy = new Interceptor();
                enemy.init(0, 0, ImageRepository.enemyShip3);
                pool[i] = enemy;
            }
        }
        else if (object == "enemyLaser") {
            for (var i = 0; i < size; i++) {
                var bullet = new Bullet("enemyLaser");
                bullet.init(0, 0, ImageRepository.enemyLaser1);
                pool[i] = bullet;
            }
        }
        else if (object == "weaponCrate") {
            for (var i = 0; i < size; i++) {
                var crate = new WeaponCrate("weaponCrate");
                crate.init(0, 0, ImageRepository.weaponCrate);
                pool[i] = crate;
            }
        }
        else if (object == "powerCrate") {
            for (var i = 0; i < size; i++) {
                var crate = new PowerUpCrate("powerCrate");
                crate.init(0, 0, ImageRepository.powerUpCrate);
                pool[i] = crate;
            }
        }
        else if (object == "explosion") {
            for (var i = 0; i < size; i++) {
                var effect = new Explosion();
                effect.init(0, 0, ImageRepository.explosion1);
                pool[i] = effect;
            }
        }
        else if (object == "asteroidExplosion") {
            for (var i = 0; i < size; i++) {
                var effect = new AsteroidExplosion();
                effect.init(0, 0, ImageRepository.explosion2);
                pool[i] = effect;
            }
        }
        else if (object == "asteroid") {
            for (var i = 0; i < size; i++) {
                var asteroid = new Asteroid();
                asteroid.getRandomAsteroid();
                asteroid.init(0, 0, asteroid.image);
                pool[i] = asteroid;
            }
        } else if (object == "asteroidGrey") {
            for (var i = 0; i < size; i++) {
                var asteroid = new AsteroidMetal();
                asteroid.getRandomAsteroid();
                asteroid.init(0, 0, asteroid.image);
                pool[i] = asteroid;
            }
        } else if (object == "boss") {
            for (var i = 0; i < size; i++) {
                var enemy = new Boss();
                enemy.init(0, 0, ImageRepository.enemyShip4);
                pool[i] = enemy;
            }
        }
    };

    this.getPool = function() {
        var obj = [];
        for (var i = 0; i < size; i++) {
            if (pool[i].alive) {
                obj.push(pool[i]);
            }
        }
        return obj;
    }

    /*
     * Grabs the last item in the list and initializes it and
     * pushes it to the front of the array.
     */
    this.get = function(x, y, speedX, speedY) {

        if(!pool[size - 1].alive) {
            pool[size - 1].spawn(x, y, speedX, speedY);
            pool.unshift(pool.pop());
        }
    };

    /*
     * Grabs the last item in the list and initializes it and
     * pushes it to the front of the array.
     */
    this.get = function(x, y, speedX, speedY, item) {

        if(!pool[size - 1].alive) {
            pool[size - 1].spawn(x, y, speedX, speedY, item);
            pool.unshift(pool.pop());
        }
    };

    /*
     * Draws any in use Bullets. If a bullet goes off the screen,
     * clears it and pushes it to the front of the array.
     */
    this.animate = function() {

        for (var i = 0; i < size; i++) {
            // Only draw until we find a bullet that is not alive
            if (pool[i].alive) {
                if (pool[i].tick()) {
                    pool[i].clear();
                    pool.push((pool.splice(i, 1))[0]);
                }
            }
        }
    };
}