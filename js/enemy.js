/**
 * Create the Destructible ship object. Represents enemies and anything that
 * player can encounter on his way.
 */
function Destructible() {

    this.chanceToFire = 0.1;
    this.score = 0;
    this.alive = false;
    this.immortal = false;
    this.exploding = false;
    this.ignoreCollision = false;
    this.health = 1;
    this.maximumHealth = 1;
    
    /*
     * Sets the Destructible values
     */
    this.spawn = function(x, y, speedX, speedY) {
        this.x = x;
        this.y = y - this.height;
        this.speedX = speedX;
        this.speedY = speedY;
        this.alive = true;
        this.health = this.maximumHealth;
    };
    
    /*
     * Move the enemy
     */
    this.tick = function() {
        this.move();
        this.draw();
        if (this.alive) {
            this.objectLogic();
        }
        return !this.alive;
    }

    this.move = function() {
        this.y += this.speedY;
        this.x += this.speedX;
    }

    this.draw = function() {
        if (this.immortal) {
            this.drawImage(this.image);
            return;
        }
        if (this.isColliding) {
            return;
        }
        if (this.y >= this.canvasHeight || this.x > this.canvasWidth) {
            this.alive = false;
        } else {
            this.drawImage(this.image);
        }
    }

    this.objectLogic = function() {
        if (this.canShoot) {
            if (Math.random() < this.chanceToFire) {
                this.fire();
            }
        }
        if (this.isColliding) {
            this.onHit();
        }
        if (this.health <= 0) {
            this.onDeath();
        }
    };

    this.onHit = function() {
        this.isColliding = false;
        this.health -= 1;
    };
    
    this.onDeath = function() {
        if (this.exploding) {
            ObjectPools.explosionAudio.get();
            ObjectPools.explosion.get(this.x, this.y, this.speedX, this.speedY, this.image);
        }
        this.alive = false;
        game.playerScore += this.score;
    };

    /*
     * Fires a bullet
     */
    this.fire = function() {
        ObjectPools.enemyLaser.get(this.x + this.width/2, this.y + this.height, 0, this.projectileSpeed, 0);
        ObjectPools.enemyLaserAudio.get();
    };

    /*
     * Resets the enemy values
     */
    this.clear = function() {
        this.x = 0;
        this.y = 0;
        this.speed = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.alive = false;
        this.isColliding = false;
        this.health = this.maximumHealth;
    };
}

/* Common enemy - medium speed, low health and low chance to fire weapons. */
function Frigate() {
    this.maximumHealth = 4;
    this.score = 30;
    this.projectileSpeed = 6;
    this.chanceToFire = 0.009;
    this.canShoot = true;
    this.exploding = true;
}

/* Strong, slow ship - stops at random Y and stays there untill destroyed. */
function Destroyer() {

    this.maximumHealth = 24;
    this.score = 60;
    this.projectileSpeed = 9;
    this.chanceToFire = 0.013;
    this.canShoot = true;
    this.stop = false;
    this.stopY = 50 + Math.floor(Math.random() * 200);
    this.goingRight = true;
    this.exploding = true;

    this.move = function() {

        if (this.stop == false) {
            this.y += this.speedY;
            if (this.y > this.stopY) {
                this.stop = true;
            }
        } else {
            if (this.x + this.width >= this.canvasWidth) {
                this.goingRight = false;
            }
            if (this.x <= 0) {
                this.goingRight = true;
            }
            if (this.goingRight) {
                this.x += this.speedX;
            } else {
                this.x -= this.speedX;
            }
        }
    }

    /*
     * Resets the enemy values
     */
    this.clear = function() {
        this.x = 0;
        this.y = 0;
        this.speed = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.alive = false;
        this.isColliding = false;
        this.stop = false;
        this.stopY = 50 + Math.floor(Math.random() * 200);
        this.health = this.maximumHealth;
    };
}

/* Fast enemy with high change to fire weapons. */
function Interceptor() {

    this.maximumHealth = 8;
    this.score = 50;
    this.projectileSpeed = 7;
    this.chanceToFire = 0.03;
    this.canShoot = true;
    this.exploding = true;
}

/* Final boss - high HP, shoots two lasers at once. */
function Boss() {

    this.maximumHealth = 280;
    this.score = 1000;
    this.projectileSpeed = 10;
    this.chanceToFire = 0.01;
    this.stopY = 20;

    /*
     * Fires a bullet
     */
    this.fire = function() {
        ObjectPools.enemyLaser.get(this.x + this.width/2 - 52, this.y + this.height, 0, this.projectileSpeed, 0);
        ObjectPools.enemyLaser.get(this.x + this.width/2 + 52, this.y + this.height, 0, this.projectileSpeed, 0);
        ObjectPools.enemyLaserAudio.get();
    };

    this.onDeath = function() {
        if (this.exploding) {
            ObjectPools.explosionAudio.get();
            ObjectPools.explosion.get(this.x, this.y, this.speedX, this.speedY, this.image);
        }
        this.alive = false;
        director.level3.victory = true;
        director.level3.bossAlive = false;
        game.playerScore += this.score;
    };
}

/* Asteroid - floating in space, innocent, but can be destroyed. */
function Asteroid() {

    this.exploding = true;
    this.score = 5;
    this.maximumHealth = 2;
    this.canShoot = false;

    this.getRandomAsteroid = function() {

        var chance = Math.random();
        var strongThreshold = 0.4;

        if (chance < 0.1) {
            this.image = ImageRepository.asteroidBigBrown1;
        } else if (chance < 0.2) {
            this.image = ImageRepository.asteroidBigBrown2;
        } else if (chance < 0.3) {
            this.image = ImageRepository.asteroidBigBrown3;
        } else if (chance < 0.4) {
            this.image = ImageRepository.asteroidBigBrown4;
        } else if (chance < 0.55) {
            this.image = ImageRepository.asteroidSmallBrown1;
        } else if (chance < 0.7) {
            this.image = ImageRepository.asteroidSmallBrown2;
        } else if (chance < 0.85) {
            this.image = ImageRepository.asteroidSmallBrown3;
        } else {
            this.image = ImageRepository.asteroidSmallBrown4;
        }
        if (chance > strongThreshold) {
            this.health = 3;
        }
    }

    this.onDeath = function() {
        if (this.exploding) {
            ObjectPools.explosionAudio.get();
            ObjectPools.asteroidExplosion.get(this.x, this.y, this.speedX, this.speedY, this.image);
        }
        this.alive = false;
        game.playerScore += this.score;
    }
}

/* Stronger version of the asteroid. */
function AsteroidMetal() {

    this.exploding = true;
    this.score = 15;
    this.maximumHealth = 5;
    this.canShoot = false;

    this.getRandomAsteroid = function(chance) {

        var chance = Math.random();
        var strongThreshold = 0.4;

        if (chance < 0.1) {
            this.image = ImageRepository.asteroidBigGrey1;
        } else if (chance < 0.2) {
            this.image = ImageRepository.asteroidBigGrey2;
        } else if (chance < 0.3) {
            this.image = ImageRepository.asteroidBigGrey3;
        } else if (chance < 0.4) {
            this.image = ImageRepository.asteroidBigGrey4;
        } else if (chance < 0.55) {
            this.image = ImageRepository.asteroidSmallGrey1;
        } else if (chance < 0.7) {
            this.image = ImageRepository.asteroidSmallGrey2;
        } else if (chance < 0.85) {
            this.image = ImageRepository.asteroidSmallGrey3;
        } else {
            this.image = ImageRepository.asteroidSmallGrey4;
        }
        if (chance > strongThreshold) {
            this.health = 6;
        }
    }
}

Destructible.prototype = new Drawable();
Frigate.prototype = new Destructible();
Destroyer.prototype = new Destructible();
Interceptor.prototype = new Frigate();
Boss.prototype = new Destroyer();
Asteroid.prototype = new Destructible();
AsteroidMetal.prototype = new Asteroid();

