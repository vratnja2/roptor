/* Takes care of driving events in levels like spawning enemies and objects. Also covers
   starting and restarting the game.
 */
function Director() {

    this.init = function() {

        this.initPlayer();
        this.initLevels();
    }

    this.initLevels = function() {
        this.level1 = new Level1();
        this.level1.init(AudioRepository.backgroundAudio1, 1, "Orion Nebula");
        this.level2 = new Level2();
        this.level2.init(AudioRepository.backgroundAudio2, 2, "Bleak Lands");
        this.level3 = new Level3();
        this.level3.init(AudioRepository.backgroundAudio3, 3, "The Citadel");
    }

    // Start the animation loop
    this.start = function() {
        this.currentLevel = this.level1;
        game.player.draw();
        this.currentLevel.start();
        this.displayGame();
        animate();
    };

    // Game over
    this.gameOver = function() {
        this.currentLevel.stop();
        console.log("Game over.");
        AudioRepository.gameOverAudio.currentTime = 0;
        AudioRepository.gameOverAudio.play();
        this.displayGameOver();
    };

    // Restart the game
    this.restart = function() {
        AudioRepository.gameOverAudio.pause();
        console.log("Restarting game.");
        this.currentLevel = this.level1;
        this.leaveWarp();
        this.clearCanvas();
        this.displayGame();
        this.initPlayer();
        ObjectPools.init();
        this.checkHighscore();
        game.playerScore = 0;
        this.start();
    };
    
    this.enterWarp = function(text) {
        AudioRepository.warpDrive.play();
        game.player.warpActive = true;
        game.background.enteringWarp = true;
        game.background2.enteringWarp = true;
        game.background3.enteringWarp = true;
        game.warp.style.display = "block";
        game.warp.innerHTML = text;
        ObjectPools.init();
    };

    this.leaveWarp = function() {
        game.player.warpActive = false;
        game.background.leavingWarp = true;
        game.background2.leavingWarp = true;
        game.background3.leavingWarp = true;
        game.warp.style.display = "none";
    };

    this.transitionTo = function(level) {
        this.currentLevel = level;
        this.currentLevel.start();
    };

    this.clearCanvas = function() {
        game.bgContext.clearRect(0, 0, game.bgCanvas.width, game.bgCanvas.height);
        game.bgContext2.clearRect(0, 0, game.bgCanvas2.width, game.bgCanvas2.height);
        game.shipContext.clearRect(0, 0, game.shipCanvas.width, game.shipCanvas.height);
        game.mainContext.clearRect(0, 0, game.mainCanvas.width, game.mainCanvas.height);
        game.background.init(0, 0, 0.4);
        game.background2.init(0, 0, 1.5);
        game.background3.init(0, -250, 0.1);
    };

    this.initPlayer = function() {
        var shipStartX = game.shipCanvas.width/2 - ImageRepository.playerShip1.width/2;
        var shipStartY = game.shipCanvas.height/4 * 2 + ImageRepository.playerShip1.height * 2;
        game.player.init(shipStartX, shipStartY, ImageRepository.playerShip1);
    };

    /* If Local Storage is available, saves highscore into it. */
    this.checkHighscore = function() {

        if (typeof(Storage) !== "undefined") {
            if (localStorage.getItem('highscore') === null) {
                localStorage.setItem('highscore', 0);
            }
            if (game.playerScore > localStorage.highscore) {
                localStorage.setItem('highscore', game.playerScore);
            }
        }
    }
    
    this.displayGame = function() {
        game.scoreBoard.style.display = "block";
        game.healthBoard.style.display = "block";
        game.powerBoard.style.display = "block";
        game.loadingBoard.style.display = "none";
        game.gameOverBoard.style.display = "none";
        game.victoryBoard.style.display = "none";
        game.startAgainBoard.style.display = "none";
    };

    this.displayGameOver = function() {
        game.scoreBoard.style.display = "block";
        game.healthBoard.style.display = "block";
        game.powerBoard.style.display = "block";
        game.gameOverBoard.style.display = "block";
        game.loadingBoard.style.display = "none";
        game.victoryBoard.style.display = "none";
        game.startAgainBoard.style.display = "none";
    };

    this.displayVictory = function() {
        document.getElementById('victoryScore').innerHTML = game.playerScore;
        if (typeof(Storage) !== "undefined") {
            document.getElementById('highScore').innerHTML = localStorage.highscore;
        } else {
            document.getElementById('highScore').innerHTML = game.playerScore;
        }
        game.scoreBoard.style.display = "none";
        game.healthBoard.style.display = "none";
        game.powerBoard.style.display = "none";
        game.gameOverBoard.style.display = "none";
        game.loadingBoard.style.display = "none";
        game.victoryBoard.style.display = "block";
        game.startAgainBoard.style.display = "block";
    };
}