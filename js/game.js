/**
 * Creates the Game object which will hold all objects and data for
 * the game.
 */
function Game() {

    /*
     * Gets canvas information and context and sets up all game objects.
     * Returns true if the canvas is supported and false if it
     * is not. This is to stop the animation script from constantly
     * running on browsers that do not support the canvas.
     */
    this.initialized = false;

    this.init = function() {
        
        this.playerScore = 0;
        this.initialized = false;
        console.log("Initializing Roptor v1.3");
        this.initHtmlElements();
        
        if (this.bgCanvas.getContext) {
            this.setCanvas();
            this.background = new Background(ImageRepository.background1);
            this.background.init(0, 0, 0.4);
            this.background2 = new Background(ImageRepository.paralaxBackground1);
            this.background2.init(0, 0, 1.5);
            this.background3 = new Background(ImageRepository.planetBackground1);
            this.background3.init(0, -150, 0.1);
            this.background3.repeat = false;

            this.player = new Player();
            WeaponRepository.init();
            director.init();
            ObjectPools.init();
            this.initialized = true;
            return true;
        } else {
            return false;
        }
    };

    this.initHtmlElements = function() {
        this.bgCanvas = document.getElementById('background');
        this.bgCanvas2 = document.getElementById('background2');
        this.bgCanvas3 = document.getElementById('background3');
        this.shipCanvas = document.getElementById('ship');
        this.mainCanvas = document.getElementById('main');
        this.scoreBoard = document.getElementById('scoreBoard');
        this.warp = document.getElementById('warp');
        this.health = document.getElementById('health');
        this.healthBoard = document.getElementById('healthBoard');
        this.powerBoard = document.getElementById('powerBoard');
        this.loadingBoard = document.getElementById('loadingBoard');
        this.score = document.getElementById('score');
        this.gameOverBoard = document.getElementById('gameOver');
        this.victoryBoard = document.getElementById('victory');
        this.startAgainBoard = document.getElementById('startAgain');
    };

    this.setCanvas = function() {
        this.bgContext = this.bgCanvas.getContext('2d');
        this.bgContext2 = this.bgCanvas2.getContext('2d');
        this.bgContext3 = this.bgCanvas3.getContext('2d');
        this.shipContext = this.shipCanvas.getContext('2d');
        this.mainContext = this.mainCanvas.getContext('2d');

        Background.prototype.context = this.bgContext;
        Background.prototype.canvasWidth = this.bgCanvas.width;
        Background.prototype.canvasHeight = this.bgCanvas.height;
        Player.prototype.context = this.shipContext;
        Player.prototype.canvasWidth = this.shipCanvas.width;
        Player.prototype.canvasHeight = this.shipCanvas.height;
        Bullet.prototype.context = this.mainContext;
        Bullet.prototype.canvasWidth = this.mainCanvas.width;
        Bullet.prototype.canvasHeight = this.mainCanvas.height;
        Destructible.prototype.context = this.mainContext;
        Destructible.prototype.canvasWidth = this.mainCanvas.width;
        Destructible.prototype.canvasHeight = this.mainCanvas.height;
        Explosion.prototype.context = this.mainContext;
        Explosion.prototype.canvasWidth = this.mainCanvas.width;
        Explosion.prototype.canvasHeight = this.mainCanvas.height;
    }
}

/**
 * The animation loop. Calls the requestAnimationFrame shim to
 * optimize the game loop and draws all game objects. This
 * function must be a global function and cannot be within an
 * object.
 */
function animate() {
    tick();
    if (game.player.health > 0) {
        requestAnimFrame(animate);
    }
    game.background.draw();
    game.background3.draw();
    game.background2.draw();

    game.mainContext.clearRect(0, 0, game.mainCanvas.width, game.mainCanvas.height);
    game.player.move();
    ObjectPools.animate();
};

function tick() {
    if (director.currentLevel.running == false) {
        return;
    }
    director.currentLevel.tick();
    game.score.innerHTML = game.playerScore;
    game.health.innerHTML = game.player.getHealthText();
    detectCollision();
    if (game.player.health <= 0) {
        game.health.innerHTML = game.player.getHealthText();
        director.gameOver();
    }
};

function detectCollision() {

    /* Detect collision of enemy with player lasers. */
    enemy = game.enemyPool.getPool();
    enemy2 = game.enemyDestroyerPool.getPool();
    enemy3 = game.enemyInterceptorPool.getPool();
    enemy4 = game.enemyBossPool.getPool();
    asteroids = game.asteroidPool.getPool();
    asteroidsGrey = game.asteroidGreyPool.getPool();
    stationary = game.weaponCratePool.getPool();
    stationary2 = game.powerCratePool.getPool();
    enemies = enemy.concat(enemy2);
    enemies = enemies.concat(enemy3);
    enemies = enemies.concat(enemy4);
    enemies = enemies.concat(asteroids);
    enemies = enemies.concat(asteroidsGrey);
    enemies = enemies.concat(stationary);
    enemies = enemies.concat(stationary2);
    lasers = game.player.bulletPool.getPool();

    for (var i = 0; i < enemies.length; i++) {
        for (var j = 0; j < lasers.length; j++) {
            if (isInCollision(enemies[i], lasers[j], enemies[i].ignoreCollision)) {
            }
        }
    }

    /* Detect collision of player with enemy asteroids. */
    asteroids = game.asteroidPool.getPool();
    asteroidsGrey = game.asteroidGreyPool.getPool();
    asteroids = asteroids.concat(asteroidsGrey);

    for (var i = 0; i < asteroids.length; i++) {
        if (isInCollision(asteroids[i], game.player, asteroids[i].ignoreCollision)) {
            var damage = 2;
            game.player.onHit(damage);
        }
    }

    /* Detect collision of player with enemy lasers. */
    lasers = game.enemyLaserPool.getPool();

    for (var i = 0; i < lasers.length; i++) {
        if (isInCollision(lasers[i], game.player, lasers[i].ignoreCollision)) {
            var damage = 1;
            game.player.onHit(damage);
        }
    }

    /* Detect collision of player with battlepickups. */
    stationary = game.weaponCratePool.getPool();
    stationary2 = game.powerCratePool.getPool();
    stationary = stationary.concat(stationary2);

    for (var i = 0; i < stationary.length; i++) {
        if (stationary[i].opened) {
            if (isInCollision(stationary[i], game.player, false)) {
                stationary[i].pickedUp = true;
                game.player.pickUp(stationary[i]);
            }
        }
    }
};

function isInCollision(obj1, obj2, ignoreCollision) {
    if (ignoreCollision) {
        return false;
    }
    if (obj1.x < obj2.x + obj2.width &&
        obj1.x + obj1.width > obj2.x &&
        obj1.y < obj2.y + obj2.height &&
        obj1.y + obj1.height > obj2.y) {
        obj1.isColliding = true;
        obj2.isColliding = true;
        return true;
    }
    return false;
};

/**
 * Ensure the game assets are loaded before starting the game
 */
var dots = 1;

function checkReadyState() {
    var loading = document.getElementById('loading');
    loadingMessage = "LOADING";
    for (var i = 0; i < dots; i++) {
        loadingMessage += ".";
    }
    dots += 1;
    if (dots > 5) {
        dots = 1;
    }
    loading.innerHTML = loadingMessage;
    if (AudioRepository.gameOverAudio.readyState === 4 &&
        AudioRepository.backgroundAudio1.readyState === 4 &&
        AudioRepository.backgroundAudio2.readyState === 4 &&
        AudioRepository.backgroundAudio3.readyState === 4 &&
        ImageRepository.ready &&
        game.initialized) {
        console.log("Game starting.");
        window.clearInterval(game.ready);
        director.start();
    }
};

/**
 * requestAnim shim layer by Paul Irish
 * Finds the first API that works to optimize the animation loop,
 * otherwise defaults to setTimeout().
 */
window.requestAnimFrame = (function(){
    return window.requestAnimationFrame   ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame    ||
        window.oRequestAnimationFrame      ||
        window.msRequestAnimationFrame     ||
        function(callback, element){
            window.setTimeout(callback, 1000 / 60);
        };
})();