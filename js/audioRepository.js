/**
 * Define an object to hold all our images for the game so images
 * are only ever created once. This type of object is known as a
 * singleton.
 */
var AudioRepository = new function() {

    this.volume = .25;
    this.bgvolume = .15;

    this.load = function() {

        // Set images src
        this.loadIngameAudio();
        this.loadFxAudio();
        this.loadBackgroundAudio();
    }

    this.loadIngameAudio = function() {

        this.weaponUp = new Audio("asset/audio/powerUp2.mp3");
        this.weaponUp.volume = this.volume * 2.5;
        this.weaponUp.load();

        this.healthUp = new Audio("asset/audio/threeTone2.mp3");
        this.healthUp.volume = this.volume * 2.5;
        this.healthUp.load();

        this.warpDrive = new Audio("asset/audio/warpdriveactive.mp3");
        this.warpDrive.volume = this.volume / 2;
        this.warpDrive.load();
    }

    this.loadBackgroundAudio = function() {

        this.backgroundAudio1 = new Audio("asset/audio/background2.mp3");
        this.backgroundAudio1.volume = this.bgvolume;
        this.backgroundAudio1.loop = true;
        this.backgroundAudio1.load();

        this.backgroundAudio2 = new Audio("asset/audio/background5.mp3");
        this.backgroundAudio2.volume = this.bgvolume + 0.1;
        this.backgroundAudio2.loop = true;
        this.backgroundAudio2.load();

        this.backgroundAudio3 = new Audio("asset/audio/background4.mp3");
        this.backgroundAudio3.volume = this.bgvolume;
        this.backgroundAudio3.loop = true;
        this.backgroundAudio3.load();

        this.gameOverAudio = new Audio("asset/audio/background3.mp3");
        this.gameOverAudio.volume = this.bgvolume;
        this.gameOverAudio.loop = true;
        this.gameOverAudio.load();
    }

    this.loadFxAudio = function() {

        this.explosionAudio1 = new Audio("asset/audio/explosion1.mp3");
        this.explosionAudio1.volume = this.bgvolume;
        this.explosionAudio1.load();

        this.explosionAudio2 = new Audio("asset/audio/explosion2.mp3");
        this.explosionAudio2.volume = this.bgvolume;
        this.explosionAudio2.load();

        this.explosionAudio3 = new Audio("asset/audio/explosion3.mp3");
        this.explosionAudio3.volume = this.bgvolume;
        this.explosionAudio3.load();

        this.explosionAudio4 = new Audio("asset/audio/explosion4.mp3");
        this.explosionAudio4.volume = this.bgvolume;
        this.explosionAudio4.load();

    }

    this.getPlayerLaserAudio = function() {
        audio = new Audio("asset/audio/laser4.mp3");
        audio.volume = this.volume * 0.9;
        audio.load();
        return audio ;
    }

    this.getEnemyLaserAudio = function() {
        audio = new Audio("asset/audio/laser3.mp3");
        audio.volume = this.volume * 1.1;
        audio.load();
        return audio ;
    }

    this.getExplosionAudio = function() {
        var rand = Math.random();
        if (rand < 0.2) {
            return this.explosionAudio1;
        } else if (rand < 0.4) {
            return this.explosionAudio2;
        } else if (rand < 0.6) {
            return this.explosionAudio3;
        } else {
            return this.explosionAudio4;
        }
    }

    this.load();
}