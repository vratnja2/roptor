/**
 * Creates the Bullet object which the ship fires. The bullets are
 * drawn on the "main" canvas.
 */
function Bullet(object) {

    this.alive = false; // Is true if the bullet is currently in use
    var self = object;

    /*
     * Sets the bullet values
     */
    this.spawn = function(x, y, speedX, speedY, rotation) {
        this.x = x;
        this.y = y;
        this.speedX = speedX;
        this.speedY = speedY;
        this.rotation = rotation;
        this.alive = true;
    };

    this.tick = function() {
        this.move();
        this.draw();
        return !this.alive;
    }

    this.move = function() {
        this.clearImage();
        this.y += this.speedY;
        this.x += this.speedX;
    }

    /*
     * Uses a "drity rectangle" to erase the bullet and moves it.
     * Returns true if the bullet moved off the screen, indicating that
     * the bullet is ready to be cleared by the pool, otherwise draws
     * the bullet.
     */
    this.draw = function() {
        if (this.isColliding || this.y <= 0 - this.height || this.y >= this.canvasHeight) {
            this.alive = false;
            return;
        }
        this.drawImage(this.image, 0, 0);
    };
    
    /*
     * Resets the bullet values
     */
    this.clear = function() {
        this.x = 0;
        this.y = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.alive = false;
        this.isColliding = false;
    };
}

Bullet.prototype = new Drawable();