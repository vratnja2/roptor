
function Explosion() {

    this.offsetX = 100;
    this.offsetY = 100;
    this.rows = 7;
    this.columns = 6;
    this.animationSpeed = 1;
    this.explosionImage = ImageRepository.explosion1;

    /*
     * Sets the Destructible values
     */
    this.spawn = function(x, y, speedX, speedY, origin) {
        this.x = x;
        this.y = y;
        this.height = origin.height;
        this.width = origin.width;
        this.speedX = speedX;
        this.speedY = speedY;
        this.alive = true;
        this.timer = 0;
        this.T = Date.now();
        this.stepX = 0;
        this.stepY = 0;
    };

    /*
     * Move the enemy
     */
    this.tick = function() {
        this.draw();
        return !this.alive;
    }

    this.draw = function() {

        this.t = Date.now();
        this.timer += (this.t - this.T) * this.animationSpeed;
        this.T = this.t;
        if(this.timer > 20) {

            this.stepX += 1;
            if (this.stepX == this.columns) {
                this.stepX = 0;
                this.stepY += 1;
            }
            if (this.stepY > this.rows) {
                this.alive = false;
            }
            this.timer = 0;
        }
        this.context.drawImage(this.explosionImage, this.offsetX * this.stepX, this.offsetY * this.stepY, this.offsetX, this.offsetY, this.x, this.y, this.width, this.height);
    }

    /*
     * Resets the values
     */
    this.clear = function() {
        this.x = 0;
        this.y = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.alive = false;
    };
}

function AsteroidExplosion() {

    this.offsetX = 64;
    this.offsetY = 64;
    this.rows = 5;
    this.columns = 5;
    this.explosionImage = ImageRepository.explosion3;
}


Explosion.prototype = new Drawable();
AsteroidExplosion.prototype = new Explosion();