/**
 * Define an object to hold all our images for the game so images
 * are only ever created once. This type of object is known as a
 * singleton.
 */
var ImageRepository = new function() {

    this.ready = false;
    this.numLoaded = 0;
    this.numImages = 42;

    this.imageLoaded = function() {
        this.numLoaded++;
        if (this.numLoaded === this.numImages) {
            console.log("Images loaded.");
            this.ready = true;
            init();
        }
    };

    this.load = function() {

        // Set images src
        this.loadPowerUps();
        this.loadBackground();
        this.loadPlayer();
        this.loadWeapon();
        this.loadEnemy();
        this.loadEnviroment();
        this.loadFx();
    };

    this.loadBackground = function() {
        this.background1 = new Image();
        this.background1.onload = function() {ImageRepository.imageLoaded()};
        this.background1.src = "asset/background/black.png";
        this.background2 = new Image();
        this.background2.onload = function() {ImageRepository.imageLoaded()};
        this.background2.src = "asset/background/blue.png";
        this.background3 = new Image();
        this.background3.onload = function() {ImageRepository.imageLoaded()};
        this.background3.src = "asset/background/darkPurple.png";

        this.paralaxBackground1 = new Image();
        this.paralaxBackground1.onload = function() {ImageRepository.imageLoaded()};
        this.paralaxBackground1.src = "asset/background/paralaxBg1.png";

        this.planetBackground1 = new Image();
        this.planetBackground1.onload = function() {ImageRepository.imageLoaded()};
        this.planetBackground1.src = "asset/background/planetBg1.png";

        this.planetBackground2 = new Image();
        this.planetBackground2.onload = function() {ImageRepository.imageLoaded()};
        this.planetBackground2.src = "asset/background/planetBg2.png";

        this.planetBackground3 = new Image();
        this.planetBackground3.onload = function() {ImageRepository.imageLoaded()};
        this.planetBackground3.src = "asset/background/planetBg3.png";
    }

    this.loadPlayer = function() {
        this.playerShip1 = new Image();
        this.playerShip1.onload = function() {ImageRepository.imageLoaded()};
        this.playerShip1.src = "asset/ship/playerShip1.png";
    }

    this.loadEnemy = function() {
        this.enemyShip1 = new Image();
        this.enemyShip1.onload = function() {ImageRepository.imageLoaded()};
        this.enemyShip1.src = "asset/ship/enemyRed1.png";
        this.enemyShip2 = new Image();
        this.enemyShip2.onload = function() {ImageRepository.imageLoaded()};
        this.enemyShip2.src = "asset/ship/enemyRed4.png";
        this.enemyShip3 = new Image();
        this.enemyShip3.onload = function() {ImageRepository.imageLoaded()};
        this.enemyShip3.src = "asset/ship/enemyRed5.png";
        this.enemyShip4 = new Image();
        this.enemyShip4.onload = function() {ImageRepository.imageLoaded()};
        this.enemyShip4.src = "asset/ship/enemyBlue3.png";
    }

    this.loadWeapon = function() {
        this.laser1 = new Image();
        this.laser1.onload = function() {ImageRepository.imageLoaded()};
        this.laser1.src = "asset/ship/laserGreen05.png";

        this.enemyLaser1 = new Image();
        this.enemyLaser1.onload = function() {ImageRepository.imageLoaded()};
        this.enemyLaser1.src = "asset/ship/laserRed03.png";

    }

    this.loadEnviroment = function() {
        this.asteroidBigBrown1 = new Image();
        this.asteroidBigBrown1.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigBrown1.src = "asset/enviroment/meteorBrown_big1.png";
        this.asteroidBigBrown2 = new Image();
        this.asteroidBigBrown2.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigBrown2.src = "asset/enviroment/meteorBrown_big2.png";
        this.asteroidBigBrown3 = new Image();
        this.asteroidBigBrown3.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigBrown3.src = "asset/enviroment/meteorBrown_big3.png";
        this.asteroidBigBrown4 = new Image();
        this.asteroidBigBrown4.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigBrown4.src = "asset/enviroment/meteorBrown_big4.png";

        this.asteroidSmallBrown1 = new Image();
        this.asteroidSmallBrown1.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallBrown1.src = "asset/enviroment/meteorBrown_med1.png";
        this.asteroidSmallBrown2 = new Image();
        this.asteroidSmallBrown2.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallBrown2.src = "asset/enviroment/meteorBrown_med3.png";
        this.asteroidSmallBrown3 = new Image();
        this.asteroidSmallBrown3.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallBrown3.src = "asset/enviroment/meteorBrown_small1.png";
        this.asteroidSmallBrown4 = new Image();
        this.asteroidSmallBrown4.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallBrown4.src = "asset/enviroment/meteorBrown_small2.png";

        this.asteroidBigGrey1 = new Image();
        this.asteroidBigGrey1.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigGrey1.src = "asset/enviroment/meteorGrey_big1.png";
        this.asteroidBigGrey2 = new Image();
        this.asteroidBigGrey2.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigGrey2.src = "asset/enviroment/meteorGrey_big2.png";
        this.asteroidBigGrey3 = new Image();
        this.asteroidBigGrey3.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigGrey3.src = "asset/enviroment/meteorGrey_big3.png";
        this.asteroidBigGrey4 = new Image();
        this.asteroidBigGrey4.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidBigGrey4.src = "asset/enviroment/meteorGrey_big4.png";

        this.asteroidSmallGrey1 = new Image();
        this.asteroidSmallGrey1.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallGrey1.src = "asset/enviroment/meteorGrey_med1.png";
        this.asteroidSmallGrey2 = new Image();
        this.asteroidSmallGrey2.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallGrey2.src = "asset/enviroment/meteorGrey_med2.png";
        this.asteroidSmallGrey3 = new Image();
        this.asteroidSmallGrey3.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallGrey3.src = "asset/enviroment/meteorGrey_small1.png";
        this.asteroidSmallGrey4 = new Image();
        this.asteroidSmallGrey4.onload = function() {ImageRepository.imageLoaded()};
        this.asteroidSmallGrey4.src = "asset/enviroment/meteorGrey_small2.png";
    }

    this.loadPowerUps = function() {

        this.weaponCrate = new Image();
        this.weaponCrate.onload = function() {ImageRepository.imageLoaded()};
        this.weaponCrate.src = "asset/powerup/barrelGreen.png";

        this.powerUpCrate = new Image();
        this.powerUpCrate.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpCrate.src = "asset/powerup/barrelGrey.png";

        this.powerUpBoltRed = new Image();
        this.powerUpBoltRed.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpBoltRed.src = "asset/powerup/red_bolt.png";
        this.powerUpBoltRed2 = new Image();
        this.powerUpBoltRed2.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpBoltRed2.src = "asset/powerup/red_bolt2.png";
        this.powerUpBoltRed3 = new Image();
        this.powerUpBoltRed3.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpBoltRed3.src = "asset/powerup/red_bolt3.png";

        this.powerUpShield = new Image();
        this.powerUpShield.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpShield.src = "asset/powerup/green_shield.png";
        this.powerUpShield2 = new Image();
        this.powerUpShield2.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpShield2.src = "asset/powerup/green_shield2.png";
        this.powerUpShield3 = new Image();
        this.powerUpShield3.onload = function() {ImageRepository.imageLoaded()};
        this.powerUpShield3.src = "asset/powerup/green_shield3.png";
    }

    this.loadFx = function() {

        this.explosion1 = new Image();
        this.explosion1.onload = function() {ImageRepository.imageLoaded()};
        this.explosion1.src = "asset/fx/explosion1.png";

        this.explosion2 = new Image();
        this.explosion2.onload = function() {ImageRepository.imageLoaded()};
        this.explosion2.src = "asset/fx/explosion2.png";

        this.explosion3 = new Image();
        this.explosion3.onload = function() {ImageRepository.imageLoaded()};
        this.explosion3.src = "asset/fx/explosion3.png";

        this.explosion4 = new Image();
        this.explosion4.onload = function() {ImageRepository.imageLoaded()};
        this.explosion4.src = "asset/fx/explosion4.png";
    }

    this.load();
}