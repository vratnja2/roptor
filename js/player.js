/*
 *  Contains data about player and his ship. Takes care of movement,
 *  death, onHit logic, weapon firing and key inputs.
 */
function Player() {

    this.speed = 8;
    this.maximumHealth = 6;
    this.health = 1;
    this.bulletPool = new ObjectPool(30);
    this.bulletPool.init("laser");
    this.warpActive = false;
    var counter = 0;
    this.immortality = 0;

    this.init = function(x, y, image) {

        this.x = x;
        this.y = y;
        this.width = image.width;
        this.height = image.height;
        this.image = image;
        this.health = this.maximumHealth;
        this.setWeapon(WeaponRepository.inventory["Single Beam"]);
    };

    this.setWeapon = function(weapon) {
        this.weapon = weapon;
        game.powerBoard.innerHTML = weapon.name;
    };
    
    this.pickUp = function(stationary) {
        stationary.pickedUp = true;
        if (stationary.inside == 'weapon') {
            this.setWeapon(stationary.contents);
            AudioRepository.weaponUp.currentTime = 0;
            AudioRepository.weaponUp.play();
        } else if (stationary.inside == 'powerup') {
            content = stationary.contents;
            if (content.type == 'hp') {
                this.health += content.strength;
                if (this.health > this.maximumHealth) {
                    this.health = this.maximumHealth;
                }
                AudioRepository.healthUp.currentTime = 0;
                AudioRepository.healthUp.play();
            }
        }
    };

    this.draw = function() {
        this.context.drawImage(ImageRepository.playerShip1, this.x, this.y);
    };

    this.move = function() {
        if (this.immortality > 0) {
            this.immortality -= 1;
        }
        // Repaint only if player moves or dies
        if (this.health <= 0) {
            this.context.clearRect(this.x - 1, this.y - 1, this.width + 2, this.height + 2);
            return;
        }
        counter++;
        if (KEY_STATUS.left || KEY_STATUS.right || KEY_STATUS.down || KEY_STATUS.up) {
            this.context.clearRect(this.x - 1, this.y - 1, this.width + 2, this.height + 2);

            if (KEY_STATUS.left) {
                this.x -= this.speed;
                if (this.x <= 0) {
                    this.x = 0;
                }
            } else if (KEY_STATUS.right) {
                this.x += this.speed;
                if (this.x >= this.canvasWidth - this.width) {
                    this.x = this.canvasWidth - this.width;                    
                }
            }
            if (KEY_STATUS.up) {
                this.y -= this.speed;
                // Restrict ship going into the top third of the game area
                if (this.y <= this.canvasHeight / 3) {
                    this.y = this.canvasHeight / 3;
                }
            } else if (KEY_STATUS.down) {
                this.y += this.speed;
                if (this.y >= this.canvasHeight - this.height - 40) {
                    this.y = this.canvasHeight - this.height - 40;                    
                }
            }
            // Finish by redrawing the ship
            if (this.health > 0) {
                this.draw();
            }
        }
        // Create shaking effect if ship is warping between levels
        if (this.warpActive) {
            this.context.clearRect(this.x, this.y, this.width, this.height);
            randX = Math.random() * 5 - 2;
            randY = Math.random() * 5 - 2;
            this.x += Math.floor(randX);
            this.y += Math.floor(randY);
            this.draw();
        }
        if (KEY_STATUS.space && counter >= this.weapon.fireRate && this.warpActive == false) {
            this.weapon.fire();
            counter = 0;
        }
    };

    this.onHit = function(damage) {
        if (this.immortality == 0) {
            this.health -= damage;
            this.immortality = 10; // Makes player immortal for three frames
        }
    };

    this.getHealthText = function() {
        var text = "";
        for (var i = 0; i < this.health; i++) {
            text += "| ";
        }
        return text;
    };
}

Player.prototype = new Drawable();

KEY_CODES = {
    32: 'space',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down',
};

KEY_STATUS = {};
for (code in KEY_CODES) {
    KEY_STATUS[ KEY_CODES[ code ]] = false;
}

/**
 * Sets up the document to listen to onkeydown events (fired when
 * any key on the keyboard is pressed down). When a key is pressed,
 * it sets the appropriate direction to true to let us know which
 * key it was.
 */
document.onkeydown = function(e) {
    // Firefox and opera use charCode instead of keyCode to
    // return which key was pressed.
    var keyCode = (e.keyCode) ? e.keyCode : e.charCode;
    if (KEY_CODES[keyCode]) {
        e.preventDefault();
        KEY_STATUS[KEY_CODES[keyCode]] = true;
    }
};

/**
 * Sets up the document to listen to on key up events (fired when
 * any key on the keyboard is released). When a key is released,
 * it sets teh appropriate direction to false to let us know which
 * key it was.
 */
document.onkeyup = function(e) {
    var keyCode = (e.keyCode) ? e.keyCode : e.charCode;
    if (KEY_CODES[keyCode]) {
        e.preventDefault();
        KEY_STATUS[KEY_CODES[keyCode]] = false;
    }
};