/*
 * Each level contains two lists of items - index of these lists represents time at which the object inside
 * the list is supposed to spawn. Enemylist spawns enemies and Objectlist spawns objects. You can change levels
 * by seemingly transitioning between them using enterWarp / leaveWarp functions.
 */
function Level() {

    this.running = false;
    this.transitionTime = 50;

    this.init = function(music, level, name) {
        this.music = music;
        this.level = level;
        this.name = name;
        this.time = 0;
    }

    this.start = function() {
        this.loadSpawnPoints();
        this.time = 0;
        if (this.music.paused) {
            this.music.currentTime = 0;
            this.music.volume = AudioRepository.bgvolume;
            this.music.play();
        }
        this.setBackground();
        this.running = true;
    }

    this.stop = function() {
        director.level1.music.pause();
        director.level2.music.pause();
        director.level3.music.pause();
        this.running = false;
    }

    this.tick = function() {
        if (this.running) {
            this.time += 1;
            if (this.time % 10 == 0) {
                this.spawn(this.time / 10);
            }
        }
    };

    this.spawnEnemy = function(enemy) {
        enemy.pool.get(enemy.x, enemy.y, enemy.speedX, enemy.speedY);
    }

    this.spawnObject = function(object) {
        object.pool.get(object.x, object.y, object.speedX, object.speedY, object.item);
    }

    this.setBackground = function() {};
    this.spawn = function() {};
    this.loadSpawnPoints = function() {};
}

function EnemySpawn(pool, x, y, speedX, speedY) {

    this.pool = pool;
    this.x = x;
    this.y = y;
    this.speedX = speedX;
    this.speedY = speedY;
}

function ObjectSpawn(pool, x, y, speedX, speedY, item) {

    this.pool = pool;
    this.x = x;
    this.y = y;
    this.speedX = speedX;
    this.speedY = speedY;
    this.item = item;
}

function Level1() {

    var enemyList = {};
    var objectList = {};

    this.transitionTime = 350;

    this.loadSpawnPoints = function() {

        // First asteroid wave
        enemyList[8] = new EnemySpawn(game.asteroidPool, 150, 0, -0.1, 1);
        enemyList[16] = new EnemySpawn(game.asteroidPool, 550, 0, 0.2, 1.2);
        enemyList[25] = new EnemySpawn(game.asteroidPool, 300, 0, 0, 1.5);
      
        // Second asteroid wave
        enemyList[40] = new EnemySpawn(game.asteroidPool, 50, 0, 0.5, 0.8);
        enemyList[45] = new EnemySpawn(game.asteroidPool, 500, 0, -0.5, 0.7);
        enemyList[50] = new EnemySpawn(game.asteroidPool, 200, 0, 0, 2.5);
        enemyList[58] = new EnemySpawn(game.asteroidPool, 300, 0, 0, 1.5);
        enemyList[62] = new EnemySpawn(game.asteroidPool, 450, 0, -0.1, 1.5);
        enemyList[62] = new EnemySpawn(game.asteroidPool, 70, 0, 0.2, 1.4);
        enemyList[65] = new EnemySpawn(game.asteroidPool, 50, 0, -0.4, 1);
        enemyList[70] = new EnemySpawn(game.asteroidPool, 500, 0, 0.6, 0.9);
        enemyList[73] = new EnemySpawn(game.asteroidPool, 250, 0, -0.5, 1.5);
        enemyList[74] = new EnemySpawn(game.asteroidPool, 350, 0, 0.5, 1.9);

        // Asteroid shower
        enemyList[90] = new EnemySpawn(game.asteroidPool, 50, 0, 0.1, 2);
        enemyList[91] = new EnemySpawn(game.asteroidPool, 450, 0, 0, 2);
        enemyList[92] = new EnemySpawn(game.asteroidPool, 200, 0, -0.1, 2);
        enemyList[93] = new EnemySpawn(game.asteroidPool, 290, 0, 0, 2);
        enemyList[94] = new EnemySpawn(game.asteroidPool, 500, 0, 0.1, 2.1);
        enemyList[95] = new EnemySpawn(game.asteroidPool, 100, 0, 0, 1.9);
        enemyList[96] = new EnemySpawn(game.asteroidPool, 440, 0, -0.1, 2.1);
        enemyList[97] = new EnemySpawn(game.asteroidPool, 80, 0, 0, 2);
        enemyList[98] = new EnemySpawn(game.asteroidPool, 350, 0, 0, 2.1);
        enemyList[99] = new EnemySpawn(game.asteroidPool, 500, 0, -0.1, 1.9);
        enemyList[100] = new EnemySpawn(game.asteroidPool, 250, 0, 0, 1.8);
        enemyList[101] = new EnemySpawn(game.asteroidPool, 500, 0, -0.1, 1.9);
        enemyList[102] = new EnemySpawn(game.asteroidPool, 100, 0, 0, 1.8);
        enemyList[103] = new EnemySpawn(game.asteroidPool, 200, 0, 0, 1.9);
        enemyList[104] = new EnemySpawn(game.asteroidPool, 300, 0, 0, 1.8);
        enemyList[105] = new EnemySpawn(game.asteroidPool, 400, 0, 0, 1.7);
        enemyList[106] = new EnemySpawn(game.asteroidPool, 550, 0, 0, 2.2);
        enemyList[107] = new EnemySpawn(game.asteroidPool, 650, 0, 0, 2);
        enemyList[108] = new EnemySpawn(game.asteroidPool, 650, 0, -0.1, 1.5);
        enemyList[109] = new EnemySpawn(game.asteroidPool, 500, 0, 0, 1.8);
        enemyList[110] = new EnemySpawn(game.asteroidPool, 450, 0, 0, 1.9);
        enemyList[111] = new EnemySpawn(game.asteroidPool, 350, 0, 0, 1.8);
        enemyList[112] = new EnemySpawn(game.asteroidPool, 250, 0, 0.1, 1.7);
        enemyList[113] = new EnemySpawn(game.asteroidPool, 150, 0, 0, 2.2);

        // First asteroid grey wave
        enemyList[130] = new EnemySpawn(game.asteroidGreyPool, 150, 0, -0.1, 1);
        enemyList[132] = new EnemySpawn(game.asteroidGreyPool, 550, 0, 0.2, 1.2);
        enemyList[134] = new EnemySpawn(game.asteroidGreyPool, 300, 0, 0, 1.5);

        objectList[150] = new ObjectSpawn(game.weaponCratePool, 450, 0, 0, 1.2, WeaponRepository.inventory["Dual Beam"]);
        objectList[170] = new ObjectSpawn(game.powerCratePool, 120, 0, 0, 1, PowerupRepository.inventory["Single Health"]);
        objectList[176] = new ObjectSpawn(game.powerCratePool, 330, 0, 0, 1, PowerupRepository.inventory["Single Health"]);

        // Second asteroid wave
        enemyList[200] = new EnemySpawn(game.asteroidPool, 50, 0, 0.5, 0.8);
        enemyList[202] = new EnemySpawn(game.asteroidGreyPool, 500, 0, -0.5, 0.7);
        enemyList[204] = new EnemySpawn(game.asteroidPool, 200, 0, 0, 2.5);
        enemyList[206] = new EnemySpawn(game.asteroidGreyPool, 300, 0, 0, 1.5);
        enemyList[208] = new EnemySpawn(game.asteroidPool, 450, 0, -0.1, 1.5);
        enemyList[210] = new EnemySpawn(game.asteroidGreyPool, 70, 0, 0.2, 1.4);
        enemyList[212] = new EnemySpawn(game.asteroidPool, 50, 0, -0.4, 1);
        enemyList[214] = new EnemySpawn(game.asteroidGreyPool, 500, 0, 0.6, 0.9);
        enemyList[216] = new EnemySpawn(game.asteroidPool, 250, 0, -0.5, 1.5);
        enemyList[218] = new EnemySpawn(game.asteroidGreyPool, 350, 0, 0.5, 1.9);

        // Asteroid highwave
        enemyList[250] = new EnemySpawn(game.asteroidPool, -120, 100, 1.6, 0.3);
        enemyList[253] = new EnemySpawn(game.asteroidPool, -120, 150, 1.7, 0.4);
        enemyList[256] = new EnemySpawn(game.asteroidPool, -120, 200, 1.8, 0.6);
        enemyList[259] = new EnemySpawn(game.asteroidPool, -120, 50, 1.6, 0.3);
        enemyList[262] = new EnemySpawn(game.asteroidPool, -120, 300, 1.6, 0.3);
        enemyList[265] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.3);
        enemyList[268] = new EnemySpawn(game.asteroidPool, -120, 50, 1.7, 0.4);
        enemyList[271] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[274] = new EnemySpawn(game.asteroidPool, -120, 250, 1.8, 0.9);
        enemyList[275] = new EnemySpawn(game.enemyPool, 500, 0, 0, 2);
        enemyList[277] = new EnemySpawn(game.asteroidPool, -120, 300, 1.6, 0.3);
        enemyList[280] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.3);
        enemyList[283] = new EnemySpawn(game.asteroidPool, -120, 50, 1.7, 0.4);
        enemyList[286] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[289] = new EnemySpawn(game.asteroidPool, -120, 250, 1.8, 0.9);
        enemyList[290] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2);

        objectList[310] = new ObjectSpawn(game.powerCratePool, 300, 0, 0, 3, PowerupRepository.inventory["Dual Health"]);
    }

    this.spawn = function(time) {

        if (typeof enemyList[time] == 'object') {
            this.spawnEnemy(enemyList[time]);
        }
        if (typeof objectList[time] == 'object') {
            this.spawnObject(objectList[time]);
        }
        if (time == this.transitionTime) {
            // start level 2
            director.enterWarp("LEVEL 2: " + director.level2.name);
            this.music.pause();
            director.level2.music.currentTime = 0;
            director.level2.music.play();
        } else if (time == this.transitionTime + 40) {
            game.background.image = ImageRepository.background2;
        }  else if (time == this.transitionTime + 60) {
            director.leaveWarp();
            game.background3.image = ImageRepository.planetBackground2;
            game.background3.y = -1650;
        } else if (time == this.transitionTime + 80) {
            director.transitionTo(director.level2);
        }
    }

    this.setBackground = function() {
        game.background.image = ImageRepository.background1;
        game.background3.image = ImageRepository.planetBackground1;
    }
}

function Level2() {

    var enemyList = {};
    var objectList = {};

    this.transitionTime = 470;

    this.loadSpawnPoints = function() {

        objectList[10] = new ObjectSpawn(game.weaponCratePool, 450, 0, 0, 2, WeaponRepository.inventory["Dual Beam"]);

        // First wave
        enemyList[25] = new EnemySpawn(game.enemyPool, 100, 0, 0, 3);
        enemyList[29] = new EnemySpawn(game.enemyPool, 600, 0, 0, 3);

        objectList[40] = new ObjectSpawn(game.powerCratePool, 200, 0, 0.2, 2, PowerupRepository.inventory["Dual Health"]);

        // Second wave
        enemyList[50] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);
        enemyList[54] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);

        // Third wave
        enemyList[70] = new EnemySpawn(game.enemyPool, 50, 0, 0, 3);
        enemyList[74] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[78] = new EnemySpawn(game.enemyPool, 350, 0, 0, 3);
        enemyList[82] = new EnemySpawn(game.enemyPool, 500, 0, 0, 3);

        // Fourth wave
        enemyList[100] = new EnemySpawn(game.enemyPool, 100, 0, 0, 3);
        enemyList[101] = new EnemySpawn(game.enemyPool, 600, 0, 0, 3);
        enemyList[110] = new EnemySpawn(game.enemyPool, 100, 0, 0, 3);
        enemyList[111] = new EnemySpawn(game.enemyPool, 600, 0, 0, 3);
        enemyList[120] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[121] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);
        enemyList[130] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[131] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);

        // First diagonal wave
        enemyList[150] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);
        enemyList[154] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);
        enemyList[158] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);
        enemyList[162] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);
        enemyList[166] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);
        enemyList[170] = new EnemySpawn(game.enemyPool, 100, 0, 2, 2);

        // Second diagonal wave
        enemyList[190] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);
        enemyList[194] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);
        enemyList[198] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);
        enemyList[202] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);
        enemyList[206] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);
        enemyList[210] = new EnemySpawn(game.enemyPool, 600, 0, -2.2, 2.2);

        objectList[220] = new ObjectSpawn(game.powerCratePool, 200, 0, 0.2, 2, PowerupRepository.inventory["Dual Health"]);
        objectList[228] = new ObjectSpawn(game.weaponCratePool, 450, 0, -0.1, 1.8, WeaponRepository.inventory["Twin Blade"]);

        // Crossing diagonals
        enemyList[250] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[254] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[258] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[262] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[266] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[270] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[274] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[278] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[282] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[286] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[290] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[294] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[290] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[294] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[298] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[302] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);

        enemyList[350] = new EnemySpawn(game.asteroidGreyPool, 50, 0, 0, 1.2);
        enemyList[352] = new EnemySpawn(game.asteroidGreyPool, 600, 0, 0, 1.2);
        enemyList[354] = new EnemySpawn(game.asteroidGreyPool, 70, 0, 0, 1);
        enemyList[356] = new EnemySpawn(game.asteroidGreyPool, 650, 0, 0, 1);
        enemyList[360] = new EnemySpawn(game.asteroidGreyPool, 80, 0, 0, 1.1);
        enemyList[362] = new EnemySpawn(game.asteroidGreyPool, 630, 0, 0, 1.2);
        enemyList[364] = new EnemySpawn(game.asteroidGreyPool, 50, 0, 0, 1);
        enemyList[366] = new EnemySpawn(game.asteroidGreyPool, 620, 0, 0, 1);
        enemyList[370] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[372] = new EnemySpawn(game.enemyPool, 300, 0, 0, 3);
        enemyList[374] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);
        enemyList[376] = new EnemySpawn(game.enemyPool, 500, 0, 0, 3);
        enemyList[378] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[380] = new EnemySpawn(game.enemyPool, 300, 0, 0, 3);
        enemyList[382] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);
        enemyList[384] = new EnemySpawn(game.enemyPool, 500, 0, 0, 3);
        enemyList[386] = new EnemySpawn(game.enemyPool, 200, 0, 0, 3);
        enemyList[388] = new EnemySpawn(game.enemyPool, 300, 0, 0, 3);
        enemyList[390] = new EnemySpawn(game.enemyPool, 400, 0, 0, 3);
        enemyList[392] = new EnemySpawn(game.enemyPool, 500, 0, 0, 3);

        enemyList[390] = new EnemySpawn(game.enemyPool, 50, 0, 0, 3);
        enemyList[391] = new EnemySpawn(game.enemyPool, 150, 0, 0, 3);
        enemyList[392] = new EnemySpawn(game.enemyPool, 550, 0, 0, 3);
        enemyList[393] = new EnemySpawn(game.enemyPool, 650, 0, 0, 3);
        enemyList[394] = new EnemySpawn(game.enemyPool, 50, 0, 0, 3);
        enemyList[395] = new EnemySpawn(game.enemyPool, 150, 0, 0, 3);
        enemyList[396] = new EnemySpawn(game.enemyPool, 550, 0, 0, 3);
        enemyList[397] = new EnemySpawn(game.enemyPool, 650, 0, 0, 3);

        objectList[420] = new ObjectSpawn(game.weaponCratePool, 700, 0, -0.5, 1.8, WeaponRepository.inventory["Twin Blade"]);
        objectList[426] = new ObjectSpawn(game.powerCratePool, 300, 0, 0.1, 2.2, PowerupRepository.inventory["Dual Health"]);
    }

    this.spawn = function(time) {

        if (typeof enemyList[time] == 'object') {
            this.spawnEnemy(enemyList[time]);
        }
        if (typeof objectList[time] == 'object') {
            this.spawnObject(objectList[time]);
        }
        if (time == this.transitionTime) {
            // start level 3
            director.enterWarp("LEVEL 3: " + director.level3.name);
        } else if (time == this.transitionTime + 5) {
            this.music.pause();
            director.level3.music.currentTime = 0;
            director.level3.music.play();
        } else if (time == this.transitionTime + 40) {
            game.background.image = ImageRepository.background3;
        } else if (time == this.transitionTime + 75) {
            director.leaveWarp();
            game.background3.image = ImageRepository.planetBackground3;
            game.background3.y = -1650;
        } else if (time == this.transitionTime + 95) {
            director.transitionTo(director.level3);
        }
    }
}

function Level3() {

    var enemyList = {};
    var objectList = {};
    var bossFightStart = 630;

    this.bossAlive = false;
    this.victory = false;
    this.victoryTime = -1;

    this.loadSpawnPoints = function() {

        this.bossAlive = false;
        this.victory = false;
        this.victoryTime = -1;

        // Asteroid highwave
        enemyList[4] = new EnemySpawn(game.asteroidGreyPool, -120, 100, 1.6, 0.3);
        enemyList[6] = new EnemySpawn(game.asteroidPool, -120, 150, 1.7, 0.4);
        enemyList[8] = new EnemySpawn(game.asteroidGreyPool, -120, 200, 1.8, 0.6);
        enemyList[10] = new EnemySpawn(game.asteroidPool, -120, 50, 1.6, 0.3);
        enemyList[12] = new EnemySpawn(game.asteroidGreyPool, -120, 300, 1.6, 0.3);
        enemyList[14] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.3);
        enemyList[16] = new EnemySpawn(game.asteroidGreyPool, -120, 50, 1.7, 0.4);
        enemyList[18] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[20] = new EnemySpawn(game.asteroidGreyPool, -120, 250, 1.8, 0.9);

        enemyList[15] = new EnemySpawn(game.enemyPool, 600, 0, -1, 3);
        enemyList[19] = new EnemySpawn(game.enemyPool, 600, 0, -1, 3);
        enemyList[23] = new EnemySpawn(game.enemyPool, 600, 0, -1, 3);
        enemyList[27] = new EnemySpawn(game.enemyPool, 600, 0, -1, 3);

        enemyList[24] = new EnemySpawn(game.asteroidGreyPool, -120, 300, 1.6, 0.5);
        enemyList[26] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.6);
        enemyList[28] = new EnemySpawn(game.asteroidGreyPool, -120, 50, 1.7, 0.6);
        enemyList[30] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[32] = new EnemySpawn(game.asteroidGreyPool, -120, 250, 1.8, 0.9);
        enemyList[34] = new EnemySpawn(game.asteroidGreyPool, -120, 100, 1.6, 1.8);

        enemyList[48] = new EnemySpawn(game.enemyPool, 100, 0, 1, 3);
        enemyList[52] = new EnemySpawn(game.enemyPool, 100, 0, 1, 3);
        enemyList[56] = new EnemySpawn(game.enemyPool, 100, 0, 1, 3);
        enemyList[60] = new EnemySpawn(game.enemyPool, 100, 0, 1, 3);

        // Fleet
        enemyList[70] = new EnemySpawn(game.enemyPool, 50, 0, 0, 3);
        enemyList[72] = new EnemySpawn(game.enemyPool, 150, 0, 0, 3);
        enemyList[74] = new EnemySpawn(game.enemyPool, 350, 0, 0, 3);
        enemyList[76] = new EnemySpawn(game.enemyPool, 550, 0, 0, 3);
        enemyList[78] = new EnemySpawn(game.enemyPool, 50, 0, 0, 3);
        enemyList[80] = new EnemySpawn(game.enemyPool, 150, 0, 0, 3);
        enemyList[82] = new EnemySpawn(game.enemyPool, 350, 0, 0, 3);
        enemyList[84] = new EnemySpawn(game.enemyPool, 550, 0, 0, 3);

        enemyList[85] = new EnemySpawn(game.asteroidGreyPool, 100, 0, 0, 0.7);
        enemyList[86] = new EnemySpawn(game.asteroidGreyPool, 300, 0, 0, 0.7);
        enemyList[87] = new EnemySpawn(game.asteroidGreyPool, 500, 0, 0, 0.7);
        enemyList[88] = new EnemySpawn(game.asteroidGreyPool, 200, 0, 0, 0.7);
        enemyList[89] = new EnemySpawn(game.asteroidGreyPool, 350, 0, 0, 0.6);
        enemyList[90] = new EnemySpawn(game.asteroidGreyPool, 50, 0, 0, 0.6);
        enemyList[91] = new EnemySpawn(game.asteroidGreyPool, 150, 0, 0, 0.6);
        enemyList[92] = new EnemySpawn(game.asteroidGreyPool, 400, 0, 0, 0.6);

        enemyList[110] = new EnemySpawn(game.enemyDestroyerPool, 100, 0, 1.5, 2);
        enemyList[120] = new EnemySpawn(game.enemyDestroyerPool, 500, 0, 1.5, 2);

        objectList[150] = new ObjectSpawn(game.powerCratePool, 400, 0, 0.1, 2.2, PowerupRepository.inventory["Triple Health"]);
        objectList[155] = new ObjectSpawn(game.weaponCratePool, 600, 0, -0.5, 1.8, WeaponRepository.inventory["Hellfire"]);

        enemyList[180] = new EnemySpawn(game.asteroidGreyPool, 50, 0, 0, 1.1);
        enemyList[181] = new EnemySpawn(game.asteroidGreyPool, 150, 0, 0.1, 1);
        enemyList[182] = new EnemySpawn(game.asteroidGreyPool, 250, 0, 0.1, 1.1);
        enemyList[183] = new EnemySpawn(game.asteroidGreyPool, 350, 0, 0, 1);
        enemyList[184] = new EnemySpawn(game.asteroidGreyPool, 450, 0, 0.1, 1);
        enemyList[185] = new EnemySpawn(game.asteroidGreyPool, 550, 0, 0, 0.9);

        enemyList[186] = new EnemySpawn(game.asteroidGreyPool, 60, 0, -0.1, 0.9);
        enemyList[187] = new EnemySpawn(game.asteroidGreyPool, 160, 0, 0, 0.8);
        enemyList[188] = new EnemySpawn(game.asteroidGreyPool, 260, 0, -0.1, 0.9);
        enemyList[189] = new EnemySpawn(game.asteroidGreyPool, 360, 0, 0, 0.9);
        enemyList[190] = new EnemySpawn(game.asteroidGreyPool, 460, 0, 0, 1);
        enemyList[191] = new EnemySpawn(game.asteroidGreyPool, 560, 0, 0.1, 0.9);

        enemyList[192] = new EnemySpawn(game.asteroidGreyPool, 30, 0, 0.1, 0.8);
        enemyList[193] = new EnemySpawn(game.asteroidGreyPool, 130, 0, 0, 0.8);
        enemyList[194] = new EnemySpawn(game.asteroidGreyPool, 230, 0, -0.1, 0.9);
        enemyList[195] = new EnemySpawn(game.asteroidGreyPool, 330, 0, 0, 0.8);
        enemyList[196] = new EnemySpawn(game.asteroidGreyPool, 430, 0, -0.1, 0.8);
        enemyList[197] = new EnemySpawn(game.asteroidGreyPool, 530, 0, 0, 0.7);

        enemyList[192] = new EnemySpawn(game.asteroidGreyPool, 70, 0, 0.1, 0.6);
        enemyList[193] = new EnemySpawn(game.asteroidGreyPool, 170, 0, -0.1, 0.5);
        enemyList[194] = new EnemySpawn(game.asteroidGreyPool, 270, 0, 0, 0.8);
        enemyList[195] = new EnemySpawn(game.asteroidGreyPool, 370, 0, 0.1, 0.7);
        enemyList[196] = new EnemySpawn(game.asteroidGreyPool, 470, 0, -0.1, 0.7);
        enemyList[197] = new EnemySpawn(game.asteroidGreyPool, 570, 0, 0, 0.8);

        enemyList[192] = new EnemySpawn(game.asteroidGreyPool, 40, 0, -0.1, 0.4);
        enemyList[193] = new EnemySpawn(game.asteroidGreyPool, 140, 0, 0, 0.6);
        enemyList[194] = new EnemySpawn(game.asteroidGreyPool, 240, 0, 0.1, 0.6);
        enemyList[195] = new EnemySpawn(game.asteroidGreyPool, 340, 0, -0.1, 0.5);
        enemyList[196] = new EnemySpawn(game.asteroidGreyPool, 440, 0, 0, 0.4);
        enemyList[197] = new EnemySpawn(game.asteroidGreyPool, 540, 0, 0.1, 0.7);

        enemyList[210] = new EnemySpawn(game.enemyDestroyerPool, 100, 0, 1.5, 1);
        enemyList[220] = new EnemySpawn(game.enemyDestroyerPool, 500, 0, 1.5, 1);

        objectList[225] = new ObjectSpawn(game.powerCratePool, 200, 0, 0.1, 2.2, PowerupRepository.inventory["Single Health"]);

        enemyList[230] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[232] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);
        enemyList[234] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[236] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);
        enemyList[238] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[240] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);
        enemyList[242] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[244] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);
        enemyList[246] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[248] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);
        enemyList[250] = new EnemySpawn(game.enemyPool, 100, 0, 0, 2.8);
        enemyList[252] = new EnemySpawn(game.enemyPool, 600, 0, 0, 2.8);

        objectList[255] = new ObjectSpawn(game.weaponCratePool, 600, 0, -0.5, 1.8, WeaponRepository.inventory["Hellfire"]);

        enemyList[260] = new EnemySpawn(game.asteroidGreyPool, 50, 0, -0.4, 1);
        enemyList[270] = new EnemySpawn(game.asteroidGreyPool, 500, 0, 0.6, 0.9);
        enemyList[280] = new EnemySpawn(game.asteroidGreyPool, 250, 0, -0.5, 1.5);
        enemyList[290] = new EnemySpawn(game.asteroidGreyPool, 350, 0, 0.5, 1.9);

        enemyList[300] = new EnemySpawn(game.enemyDestroyerPool, 350, 0, 1.5, 2);

        // Crossing diagonals
        enemyList[310] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[314] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[318] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[322] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[326] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[330] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[334] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[338] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[342] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[346] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[350] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[354] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[358] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);
        enemyList[362] = new EnemySpawn(game.enemyPool, 650, 0, -2, 2);
        enemyList[366] = new EnemySpawn(game.enemyPool, 50, 0, 2, 2);

        objectList[390] = new ObjectSpawn(game.powerCratePool, 400, 0, 0.5, 2.5, PowerupRepository.inventory["Dual Health"]);

        enemyList[440] = new EnemySpawn(game.enemyInterceptorPool, 0, 0, 4, 5);
        enemyList[450] = new EnemySpawn(game.enemyInterceptorPool, 700, 0, -4, 5);
        enemyList[460] = new EnemySpawn(game.enemyInterceptorPool, 0, 0, 5, 3);
        enemyList[465] = new EnemySpawn(game.enemyInterceptorPool, 700, 0, -5, 3);
        enemyList[470] = new EnemySpawn(game.enemyInterceptorPool, 0, 0, 5, 3);
        enemyList[475] = new EnemySpawn(game.enemyInterceptorPool, 700, 0, -5, 3);

        enemyList[490] = new EnemySpawn(game.asteroidPool, -120, 100, 1.6, 0.3);
        enemyList[493] = new EnemySpawn(game.asteroidPool, -120, 150, 1.7, 0.4);
        enemyList[496] = new EnemySpawn(game.asteroidPool, -120, 200, 1.8, 0.6);
        enemyList[499] = new EnemySpawn(game.asteroidPool, -120, 50, 1.6, 0.3);
        enemyList[502] = new EnemySpawn(game.asteroidPool, -120, 300, 1.6, 0.3);
        enemyList[505] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.3);
        enemyList[508] = new EnemySpawn(game.asteroidPool, -120, 50, 1.7, 0.4);
        enemyList[511] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[514] = new EnemySpawn(game.asteroidPool, -120, 250, 1.8, 0.9);

        enemyList[518] = new EnemySpawn(game.enemyInterceptorPool, 50, 0, 2, 5);
        enemyList[519] = new EnemySpawn(game.enemyInterceptorPool, 650, 0, -2, 5);

        enemyList[520] = new EnemySpawn(game.asteroidPool, -120, 300, 1.6, 0.3);
        enemyList[523] = new EnemySpawn(game.asteroidPool, -120, 150, 1.6, 0.3);
        enemyList[526] = new EnemySpawn(game.asteroidPool, -120, 50, 1.7, 0.4);
        enemyList[529] = new EnemySpawn(game.asteroidPool, -120, 200, 1.9, 0.8);
        enemyList[532] = new EnemySpawn(game.asteroidPool, -120, 250, 1.8, 0.9);

        enemyList[550] = new EnemySpawn(game.enemyInterceptorPool, 50, 0, 2, 5);
        enemyList[554] = new EnemySpawn(game.enemyInterceptorPool, 650, 0, -2, 5);
        enemyList[558] = new EnemySpawn(game.enemyInterceptorPool, 250, 0, 2, 5);
        enemyList[562] = new EnemySpawn(game.enemyInterceptorPool, 450, 0, -2, 5);

        objectList[580] = new ObjectSpawn(game.powerCratePool, 400, 0, 0, 2, PowerupRepository.inventory["Triple Health"]);

        enemyList[bossFightStart] = new EnemySpawn(game.enemyBossPool, 350, 0, 1.5, 1.5);
    }

    this.spawn = function(time) {

        if (typeof enemyList[time] == 'object') {
            this.spawnEnemy(enemyList[time]);
        }
        if (typeof objectList[time] == 'object') {
            this.spawnObject(objectList[time]);
        }
        if (time == bossFightStart) {
            this.bossAlive = true;
        }
        if (this.bossAlive) {
            if (time % 70 == 0) {
                this.spawnEnemy(new EnemySpawn(game.enemyInterceptorPool, 50, 0, 2, 5));
                this.spawnEnemy(new EnemySpawn(game.enemyInterceptorPool, 650, 0, -2, 5));
            }
            if (time % 80 == 10) {
                this.spawnEnemy(new EnemySpawn(game.enemyDestroyerPool, -100, 200, 2, 2));
            }
            if (time % 70 == 20) {
                this.spawnObject(new ObjectSpawn(game.powerCratePool, 100 + Math.random() * 500, 0, 0, 3, PowerupRepository.inventory["Single Health"]));
            }
        }
        if (this.victory && this.victoryTime < 0) {
            director.checkHighscore();
            this.victoryTime = time;
        }
        if (time == this.victoryTime + 20 && this.victory) {
            director.enterWarp("");
            director.displayVictory();
        }
    }
}

Level1.prototype = new Level();
Level2.prototype = new Level();
Level3.prototype = new Level();