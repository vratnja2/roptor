/**
 * A sound pool to use for the sound effects
 */
function AudioPool(maxSize) {

    var size = maxSize; // Max sounds allowed in the pool
    var pool = [];
    this.pool = pool;
    var currSound = 0;
    /*
     * Populates the pool array with the given sound
     */
    this.init = function(object) {
        if (object == "playerLaser") {
            for (var i = 0; i < size; i++) {
                pool[i] = AudioRepository.getPlayerLaserAudio();
            }
        }
        else if (object == "enemyLaser") {
            for (var i = 0; i < size; i++) {
                pool[i] = AudioRepository.getEnemyLaserAudio();
            }
        }
        else if (object == "explosionAudio") {
            for (var i = 0; i < size; i++) {
                pool[i] = AudioRepository.getExplosionAudio();
            }
        }
    };
    /*
     * Plays a sound
     */
    this.get = function() {
        if(pool[currSound].currentTime == 0 || pool[currSound].ended) {
            pool[currSound].play();
        }
        currSound = (currSound + 1) % size;
    };
}