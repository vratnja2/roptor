/**
 * Creates the Drawable object which will be the base class for
 * all drawable objects in the game. Sets up default variables
 * that all child objects will inherit, as well as the default
 * functions.
 */

function Drawable() {

    this.init = function(x, y, image) {
        // Defualt variables
        this.x = x;
        this.y = y;
        this.width = image.width;
        this.height = image.height;
        this.image = image;
    };

    this.clearImage = function() {
        this.context.clearRect(this.x - 1, this.y - 1, this.width + 2, this.height + 2);
    };

    this.drawImage = function(image) {
        this.context.drawImage(image, this.x, this.y);
    };

    this.speedX = 0;
    this.speedY = 0;
    this.canvasWidth = 700;
    this.canvasHeight = 700;

    // Define abstract function to be implemented in child objects
    this.draw = function() {};
    this.move = function() {};
}