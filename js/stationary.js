/*
 * Represents destructible item that can be picked up. First, there is container that needs to be destroyed -
 * upon destruction spawns contents that player can pick up.
 */
function Stationary(object) {

    var self = object;
    this.alive = false;
    this.opened = false;
    this.pickedUp = false;

    this.spawn = function(x, y, speedX, speedY, contents) {
        this.x = x;
        this.y = y - this.height;
        this.speedX = speedX;
        this.speedY = speedY;
        this.alive = true;
        this.health = this.maximumHealth;
        this.contents = contents;
    };

    this.objectLogic = function() {
        if (this.pickedUp) {
            this.alive = false;
            this.clear();
        }
        if (this.opened) {
            return;
        }
        if (this.isColliding) {
            this.onHit();
        }
        if (this.health <= 0) {
            this.onDeath();
        }
    };

    this.onDeath = function() {
        this.opened = true;
        this.immortal = true;
        this.ignoreCollision = true;
        this.image = this.contents.powerUpImage;
        this.width = this.image.width;
        this.height = this.image.height;
    };

    this.clear = function() {
        this.alive = false;
        this.opened = false;
        this.immortal = false;
        this.pickedUp = false;
        this.ignoreCollision = false;
        this.image = this.defaultImage;
        this.health = this.maximumHealth;
        this.isColliding = false;
    }
}

/* Stationary containing weapons. */
function WeaponCrate() {

    this.maximumHealth = 4;
    this.inside = 'weapon';
    this.defaultImage = ImageRepository.weaponCrate;
    this.powerUpImage = ImageRepository.powerUpBoltRed;
}

/* Stationary containing powerups like health. */
function PowerUpCrate() {

    this.inside = 'powerup';
    this.maximumHealth = 3;
    this.defaultImage = ImageRepository.powerUpCrate;
    this.powerUpImage = ImageRepository.powerUpShield;
}

Stationary.prototype = new Destructible();
WeaponCrate.prototype = new Stationary();
PowerUpCrate.prototype = new Stationary();
