function Powerup() {

    this.powerUpImage = ImageRepository.powerUpShield;
    // We dont have any other type in the game at this point
    this.type = "hp";
}

function SingleHealth() {

    this.name = "Single Health";
    this.strength = 1;
    this.powerUpImage = ImageRepository.powerUpShield;
}

function DualHealth() {

    this.name = "Dual Health";
    this.strength = 2;
    this.powerUpImage = ImageRepository.powerUpShield2;
}

function TripleHealth() {

    this.name = "Triple Health";
    this.strength = 3;
    this.powerUpImage = ImageRepository.powerUpShield3;
}


SingleHealth.prototype = new Powerup();
DualHealth.prototype = new Powerup();
TripleHealth.prototype = new Powerup();


var PowerupRepository = new function() {

    this.init = function() {
        this.inventory = [];
        this.inventory["Single Health"] = new SingleHealth();
        this.inventory["Dual Health"] = new DualHealth();
        this.inventory["Triple Health"] = new TripleHealth();
    }

    this.init();
}